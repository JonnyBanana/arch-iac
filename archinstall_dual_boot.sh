# Before Install
# loadkeys de-latin1
# curl -sL https://gitlab.com/JonnyBanana/arch-iac/-/raw/main/archinstall.sh > install.sh
# curl -sL https://gitlab.com/JonnyBanana/arch-iac/-/raw/main/subshell.sh > subshell.sh
# bash install.sh
# 
# Initial Setup
echo "Please enter root password."
read -s root_password
export root_password

echo "Re-enter:"
read -s root_password_confirm

# if [ "$root_password" = "$root_password_confirm" ]; then

#     timedatectl set-ntp true
#     (
#     echo o
#     echo n
#     echo p
#     echo 
#     echo
#     echo +128M
#     echo a
#     echo n
#     echo p
#     echo 
#     echo 
#     echo
#     echo w
#     )| fdisk /dev/sda

#     mkfs.ext4 /dev/sda1
#     mkfs.ext4 /dev/sda2
#     mount /dev/sda2 /mnt
#     mkdir /mnt/boot
#     mount /dev/sda1 /mnt/boot

if [ "$root_password" = "$root_password_confirm" ]; then

    pacstrap /mnt base base-devel linux linux-firmware vim
    genfstab -U /mnt >> /mnt/etc/fstab

    cp subshell.sh /mnt/
    arch-chroot /mnt /bin/bash subshell.sh

    umount -R /mnt
    reboot

fi
