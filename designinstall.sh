# pacman -S git
# git clone https://gitlab.com/JonnyBanana/arch-iac.git

echo "Please enter root password."
read -s root_password

echo "Re-enter:"
read -s root_password_confirm


if [ "$root_password" = "$root_password_confirm" ]; then

	useradd -m jonny

	(
	echo $root_password
	echo $root_password
	) | passwd jonny

	echo 'jonny ALL=(ALL:ALL) ALL' >> /etc/sudoers

	sudo -i -u jonny bash << EOF

	bash jonnyinstall.sh

EOF

	reboot

fi


