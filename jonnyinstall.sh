	bash jonnyinstall.sh
	sudo pacman -S --noconfirm xorg qtile alacritty lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings
	sudo pacman -S --noconfirm feh picom bat just zsh noto-fonts-emoji

	sudo systemctl enable lightdm

	cd ~
	git clone https://aur.archlinux.org/yay-bin.git
	cd yay-bin
	makepkg -si
	yay -S 

	cd ~
	git clone https://gitlab.com/JonnyBanana/arch-iac.git
	cd arch-iac
	cp -r configfiles/* /home/jonny/.config/

	sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
	#git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k