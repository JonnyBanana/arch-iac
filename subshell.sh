sed -i 's/#en_US/en_US/' /etc/locale.gen
locale-gen
echo "LANG=en-US.UTF-8" > /etc/locale.conf
export LC_ALL="en_US.UTF-8"
echo 'LC_CTYPE="en_US.UTF-8"' > /etc/default/locale
echo 'LC_ALL="en_US.UTF-8"' >> /etc/default/locale
echo 'LANG="en_US.UTF-8"' >> /etc/default/locale 

pacman -Sy --noconfirm networkmanager grub
systemctl enable NetworkManager

grub-install /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg

(
echo $root_password
echo $root_password
) | passwd

echo "bananabox" > /etc/hostname

ln -s /usr/share/zoneinfo/Europe/Berlin /etc/localtime
exit
